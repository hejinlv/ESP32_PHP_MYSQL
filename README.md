# ESP32_PHP_MYSQL

#### 介绍
ESP32 POST data to PHP and PHP UPDATE to mysql

#### 软件架构
ESP32 MicroPython


#### 安装教程

1.  Win安装phpstudy 或者 树莓派安装 php + mysql-server + nginx 
2.  将esp-data.php  post-esp-data.php 放入服务器主目录
3.  EPS32下载ESP32.py
4.  数据库导入SensorData_Table.sql文件或者新建一个表

#### 使用说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/0303/145656_4669b402_2242252.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0303/145729_044a091f_2242252.png "屏幕截图.png")

参考：

https://randomnerdtutorials.com/esp32-esp8266-mysql-database-php/

